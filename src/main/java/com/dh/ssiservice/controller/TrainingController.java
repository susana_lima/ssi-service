/**
 * @author: Susana Lima.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.TrainingCommand;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/trainings")
@Produces(MediaType.APPLICATION_JSON)
@CrossOrigin
public class TrainingController {
    private TrainingService trainingService;

    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @GET
    public Response getTrainings() {
        List<TrainingCommand> trainings = new ArrayList<>();
        trainingService.findAll().forEach(training -> {
            TrainingCommand trainingCommand = new TrainingCommand(training);
            trainings.add(trainingCommand);
        });

        return Response.ok(trainings).build();
    }

    @GET
    @Path("/{id}")
    public Response getTrainingsById(@PathParam("id") @NotNull Long id) {
        Training training = trainingService.findById(id);
        return Response.ok(new TrainingCommand(training)).build();
    }

    @POST
    public Response saveTraining(TrainingCommand training) {
        Training model = training.toDomain();
        Training trainingPersisted = trainingService.save(model);
        return Response.ok(new TrainingCommand(trainingPersisted)).build();
    }

    @PUT
    public Response updateTraining(Training training) {
        Training trainingPersisted = trainingService.save(training);
        return Response.ok(new TrainingCommand(trainingPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteTraining(@PathParam("id") String id) {
        trainingService.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }
}    
