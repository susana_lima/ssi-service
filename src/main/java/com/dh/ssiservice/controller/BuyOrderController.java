/**
 * @author: Susana Lima.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.BuyOrderCommand;
import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.services.BuyOrderService;
import org.springframework.stereotype.Controller;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/buyOrders")
@Produces(MediaType.APPLICATION_JSON)
@CrossOrigin
public class BuyOrderController {
    private BuyOrderService buyOrderService;

    public BuyOrderController(BuyOrderService buyOrderService) {
        this.buyOrderService = buyOrderService;
    }

    @GET
    public Response getBuyOrders() {
        List<BuyOrderCommand> buyOrders = new ArrayList<>();
        buyOrderService.findAll().forEach(buyOrder -> {
            BuyOrderCommand buyOrderCommand = new BuyOrderCommand(buyOrder);
            buyOrders.add(buyOrderCommand);
        });

        return Response.ok(buyOrders).build();
    }

    @GET
    @Path("/{id}")
    public Response getBuyOrdersById(@PathParam("id") @NotNull Long id) {
        BuyOrder buyOrder = buyOrderService.findById(id);
        return Response.ok(new BuyOrderCommand(buyOrder)).build();
    }

    @POST
    public Response saveBuyOrder(BuyOrderCommand buyOrder) {
        BuyOrder model = buyOrder.toDomain();
        BuyOrder buyOrderPersisted = buyOrderService.save(model);
        return Response.ok(new BuyOrderCommand(buyOrderPersisted)).build();
    }

    @PUT
    public Response updateBuyOrder(BuyOrder buyOrder) {
        BuyOrder buyOrderPersisted = buyOrderService.save(buyOrder);
        return Response.ok(new BuyOrderCommand(buyOrderPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteBuyOrder(@PathParam("id") String id) {
        buyOrderService.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }
}    
