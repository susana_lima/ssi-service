/**
 * @author: Susana Lima.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidentCommand;
import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.services.IncidentService;
import org.springframework.stereotype.Controller;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incidents")
@Produces(MediaType.APPLICATION_JSON)
@CrossOrigin
public class IncidentController {
    private IncidentService incidentService;

    public IncidentController(IncidentService incidentService) {
        this.incidentService = incidentService;
    }

    @GET
    public Response getIncidents() {
        List<IncidentCommand> incidents = new ArrayList<>();
        incidentService.findAll().forEach(incident -> {
            IncidentCommand incidentCommand = new IncidentCommand(incident);
            incidents.add(incidentCommand);
        });

        return Response.ok(incidents).build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentsById(@PathParam("id") @NotNull Long id) {
        Incident incident = incidentService.findById(id);
        return Response.ok(new IncidentCommand(incident)).build();
    }

    @POST
    public Response saveIncident(IncidentCommand incident) {
        Incident model = incident.toDomain();
        Incident incidentPersisted = incidentService.save(model);
        return Response.ok(new IncidentCommand(incidentPersisted)).build();
    }

    @PUT
    public Response updateIncident(Incident incident) {
        Incident incidentPersisted = incidentService.save(incident);
        return Response.ok(new IncidentCommand(incidentPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteIncident(@PathParam("id") String id) {
        incidentService.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }
}    
