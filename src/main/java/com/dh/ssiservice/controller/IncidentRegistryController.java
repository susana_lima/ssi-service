/**
 * @author: Susana Lima.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidentRegistryCommand;
import com.dh.ssiservice.model.IncidentRegistry;
import com.dh.ssiservice.services.IncidentRegistryService;
import org.springframework.stereotype.Controller;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incidentRegistries")
@Produces(MediaType.APPLICATION_JSON)
@CrossOrigin
public class IncidentRegistryController {
    private IncidentRegistryService incidentRegistryService;

    public IncidentRegistryController(IncidentRegistryService incidentRegistryService) {
        this.incidentRegistryService = incidentRegistryService;
    }

    @GET
    public Response getIncidentRegistries() {
        List<IncidentRegistryCommand> incidentRegistries = new ArrayList<>();
        incidentRegistryService.findAll().forEach(incidentRegistry -> {
            IncidentRegistryCommand incidentRegistryCommand = new IncidentRegistryCommand(incidentRegistry);
            incidentRegistries.add(incidentRegistryCommand);
        });

        return Response.ok(incidentRegistries).build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentRegistriesById(@PathParam("id") @NotNull Long id) {
        IncidentRegistry incidentRegistry = incidentRegistryService.findById(id);
        return Response.ok(new IncidentRegistryCommand(incidentRegistry)).build();
    }

    @POST
    public Response saveIncidentRegistry(IncidentRegistryCommand incidentRegistry) {
        IncidentRegistry model = incidentRegistry.toDomain();
        IncidentRegistry incidentRegistryPersisted = incidentRegistryService.save(model);
        return Response.ok(new IncidentRegistryCommand(incidentRegistryPersisted)).build();
    }

    @PUT
    public Response updateIncidentRegistry(IncidentRegistry incidentRegistry) {
        IncidentRegistry incidentRegistryPersisted = incidentRegistryService.save(incidentRegistry);
        return Response.ok(new IncidentRegistryCommand(incidentRegistryPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteIncidentRegistry(@PathParam("id") String id) {
        incidentRegistryService.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }
}    
