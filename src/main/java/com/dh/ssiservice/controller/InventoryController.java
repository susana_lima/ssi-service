/**
 * @author: Susana Lima.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.InventoryCommand;
import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.services.InventoryService;
import org.springframework.stereotype.Controller;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/inventories")
@Produces(MediaType.APPLICATION_JSON)
@CrossOrigin
public class InventoryController {
    private InventoryService inventoryService;

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GET
    public Response getInventories() {
        List<InventoryCommand> inventories = new ArrayList<>();
        inventoryService.findAll().forEach(inventory -> {
            InventoryCommand inventoryCommand = new InventoryCommand(inventory);
            inventories.add(inventoryCommand);
        });

        return Response.ok(inventories).build();
    }

    @GET
    @Path("/{id}")
    public Response getInventoriesById(@PathParam("id") @NotNull Long id) {
        Inventory inventory = inventoryService.findById(id);
        return Response.ok(new InventoryCommand(inventory)).build();
    }

    @POST
    public Response saveInventory(InventoryCommand inventory) {
        Inventory model = inventory.toDomain();
        Inventory inventoryPersisted = inventoryService.save(model);
        return Response.ok(new InventoryCommand(inventoryPersisted)).build();
    }

    @PUT
    public Response updateInventory(Inventory inventory) {
        Inventory inventoryPersisted = inventoryService.save(inventory);
        return Response.ok(new InventoryCommand(inventoryPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteInventory(@PathParam("id") String id) {
        inventoryService.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }
}    
