package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.model.Item;

public class InventoryCommand {

    private String warehouse;
    private Long itemId;
    private int quantity;
    private String status;

    public InventoryCommand(Inventory inventory){
        this.setWarehouse(inventory.getWarehouse());
        this.setQuantity(inventory.getQuantity());
        this.setStatus(inventory.getStatus());
    }

    public InventoryCommand(){}

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Inventory toDomain(){
        Inventory inventory = new Inventory();
        inventory.setWarehouse(getWarehouse());
        inventory.setQuantity(getQuantity());
        inventory.setStatus(getStatus());

        return inventory;
    }
}
