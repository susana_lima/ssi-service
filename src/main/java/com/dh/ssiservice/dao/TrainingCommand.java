package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Training;

import java.util.Date;

public class TrainingCommand {
    private String skill;
    private Date trainingDate;
    private String area;
    private String position;

    public TrainingCommand (Training training){
        this.setSkill(training.getSkill());
        this.setTrainingDate(training.getTrainingDate());
        this.setArea(training.getArea());
        this.setPosition(training.getPosition());
    }

    public TrainingCommand(){}

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public Date getTrainingDate() {
        return trainingDate;
    }

    public void setTrainingDate(Date trainingDate) {
        this.trainingDate = trainingDate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Training toDomain(){
        Training training = new Training();
        training.setSkill(getSkill());
        training.setTrainingDate(getTrainingDate());
        training.setArea(getArea());
        training.setPosition(getPosition());

        return training;
    }
}
