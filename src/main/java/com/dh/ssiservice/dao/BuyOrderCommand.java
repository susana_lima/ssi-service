package com.dh.ssiservice.dao;


import com.dh.ssiservice.model.BuyOrder;

public class BuyOrderCommand {
    private String unit;
    private String supplier;
    private String status;

    public BuyOrderCommand(BuyOrder buyOrder){
        setUnit(buyOrder.getUnit());
        setSupplier(buyOrder.getSupplier());
        setStatus(buyOrder.getStatus());
    }
    public BuyOrderCommand(){}

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BuyOrder toDomain(){
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setUnit(getUnit());
        buyOrder.setSupplier(getSupplier());
        buyOrder.setStatus(getStatus());

        return buyOrder;
    }
}
