package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.IncidentRegistry;

import java.util.Date;

public class IncidentRegistryCommand {
    private Long incidentId;
    private Date incidentDate;
    private String employee;
    private String cause;
    private String area;

    public IncidentRegistryCommand(IncidentRegistry incidentRegistry){
        this.setIncidentDate(incidentRegistry.getIncidentDate());
        this.setEmployee(incidentRegistry.getEmployee().getFirstName());
        this.setCause(incidentRegistry.getCause());
        this.setArea(incidentRegistry.getArea());
    }

    public IncidentRegistryCommand(){}

    public Long getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(Long incidentId) {
        this.incidentId = incidentId;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public IncidentRegistry toDomain(){
        IncidentRegistry incidentRegistry = new IncidentRegistry();
        incidentRegistry.setIncidentDate(getIncidentDate());
        incidentRegistry.setCause(getCause());
        incidentRegistry.setArea(getArea());

        return incidentRegistry;
    }
}
