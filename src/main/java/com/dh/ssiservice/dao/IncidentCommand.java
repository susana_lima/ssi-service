package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Incident;

public class IncidentCommand {
    private String name;
    private String code;

    public IncidentCommand(Incident incident){
        this.setCode(incident.getCode());
        this.setName(incident.getName());
    }

    public IncidentCommand(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Incident toDomain(){
        Incident incident = new Incident();
        incident.setCode(getCode());
        incident.setName(getName());

        return incident;
    }
}
