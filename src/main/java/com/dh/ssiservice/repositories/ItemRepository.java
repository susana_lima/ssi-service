package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.Item;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
