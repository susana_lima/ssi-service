package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.IncidentRegistry;

public interface IncidentRegistryRepository extends CrudRepository<IncidentRegistry, Long> {
}
