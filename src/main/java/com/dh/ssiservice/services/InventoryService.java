/**
 * @author: Edson A. Terceros T.
 */
package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Inventory;

public interface InventoryService extends GenericService<Inventory> {
}
